#!/usr/bin/perl

use strict;
use warnings;

my $src_filepath = "pf.sql";
my $dest_filepath = "db.sql";

sub fix_numbers
{
  # Get the subroutine's argument.
	my ($orig_num) = $_[0];
	my ($string) = $_[2];
	my ($statement) = $_[1] . $_[2] . $_[3];

	# Get length
	my $len = length $string;

	if(defined($len))
	{
		# Got a replacement; return it.
		return "s:" . $len . $statement;
	}

	# No replacement; return original text.
	return "s:" . $orig_num . $statement;
}

sub main
{
	open(my $SRC, '<', $src_filepath) or die "Can't open $src_filepath: $!";
	open(my $DEST, '>', $dest_filepath ) or die "Can't open file $dest_filepath to write: $!";
	while (my $line = readline($SRC)) {
	    ## ... process the line in here
		$line =~ s/s:(\d+)(:\\?\")(.*?)(\\?\";)/fix_numbers($1, $2, $3, $4)/eig;
		print {$DEST} $line;
	}
	close($SRC);
	close($DEST);
}

main();
