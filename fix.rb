#!/usr/bin/env ruby
#
# Contributed by Mic Alexander, https://github.com/micalexander
#

Encoding.default_external = Encoding::UTF_8
Encoding.default_internal = Encoding::UTF_8

def fix_serialization file

	Encoding.default_external = Encoding::UTF_8
	Encoding.default_internal = Encoding::UTF_8
	string = File.read file

	fixed = fix_text string

	open file, 'w' do |io|
	io.write fixed
	end

	return file
end

# php escapes:
# "\\" #Backslash, '"' Double quotes,    "\'" Single quotes, "\a" Bell/alert,
# "\b" Backspace,  "\r" Carriage Return, "\n" New Line,      "\s" Space,      "\t" Tab

def fix_text string
	pattern = /(s\s*:\s*)(\d+)((\s*:\\*["&])(.*?)(\\?\"\s*;))/
	php_escapes = /(\\"|\\'|\\\\|\\a|\\b|\\n|\\r|\\s|\\t|\\v)/

	string.gsub( pattern ) do |match|
		head = $1
		tail = $3

		count = $5.bytesize - $5.scan(php_escapes).length

		"#{head}#{count}#{tail}"
	end
end

# test_1 = fix_serialization "s:12:\"robots\\.txt$\";"

# raise test_1 unless "s:12:\"robots\\.txt$\";" == test_1

fix_serialization 'kd-2013-11-01-09-27-production.sql'
